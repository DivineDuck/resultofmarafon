#pragma once

#define MAX_STRING_SIZE 200
struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct date
{
    int hour;
    int minute;
    int second;
};
struct peopleofmarafon
{
    person marafonec;
    date start;
    date finish;
  
};
