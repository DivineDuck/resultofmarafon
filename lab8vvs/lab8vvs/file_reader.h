#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "acc.h"

void read(const char* file_name, peopleofmarafon* array[], int& size);

#endif